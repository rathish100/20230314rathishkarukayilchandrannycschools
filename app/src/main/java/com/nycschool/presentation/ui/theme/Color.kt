package com.nycschool.presentation.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

val BabyBlue = Color(0xFF79A9F5)
val DarkBabyBlue = Color(0xFFC2E2F5)

val DarkGray = Color(0xFF202020)
val MediumGray = Color(0xFF505050)
val ColorPrimary = Color(0xFF08FF04)
val TextWhite = Color(0xFFEEEEEE)

