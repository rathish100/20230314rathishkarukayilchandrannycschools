package com.nycschool.presentation

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.ui.Modifier
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.nycschool.presentation.school_detail.SchoolDetailScreen
import com.nycschool.presentation.school_list.SchoolListScreen
import dagger.hilt.android.AndroidEntryPoint



@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    val navController = rememberNavController()
                    NavHost(navController = navController, startDestination = Screen.SchoolListScreen.route){
                        composable(
                            route = Screen.SchoolListScreen.route
                        ){
                            SchoolListScreen(navController = navController)
                        }

                        composable(
                            route = Screen.SchoolDetailScreen.route + "/{dbn}"
                        ){
                            SchoolDetailScreen()
                        }

                    }
                }

        }
    }
}




