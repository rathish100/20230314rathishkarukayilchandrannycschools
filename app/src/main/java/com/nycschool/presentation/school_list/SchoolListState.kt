package com.nycschool.presentation.school_list

import com.nycschool.domain.model.School


data class SchoolListState(
    val isLoading: Boolean = false,
    val schoolList: List<School> = emptyList(),
    val error: String = ""
)
