package com.nycschool.presentation.school_list

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nycschool.domain.usecase.getschools.GetSchoolListUseCase
import com.nycschool.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class SchoolListViewModel @Inject constructor(
    private val getSchoolListUseCase: GetSchoolListUseCase
): ViewModel(){

    init {
        getSchoolList()
    }
    private val _state = mutableStateOf(SchoolListState())
    val state: State<SchoolListState> = _state

    private fun getSchoolList(){
        getSchoolListUseCase().onEach { result ->
            when(result){
                is Resource.Success -> {
                    _state.value = SchoolListState(schoolList = result.data ?: emptyList())
                }
                is Resource.Error -> {
                    _state.value = SchoolListState(error = result.message ?: "An Unexpected error occured")
                }
                else -> {}

            }
        }.launchIn(viewModelScope)
    }

}