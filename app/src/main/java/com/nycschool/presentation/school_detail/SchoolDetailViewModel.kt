package com.nycschool.presentation.school_detail

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nycschool.domain.usecase.getSchoolDetails.GetSchoolDetailUseCase
import com.nycschool.utils.Constants
import com.nycschool.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class SchoolDetailViewModel @Inject constructor(
    private val getSchoolDetailUseCase: GetSchoolDetailUseCase,
    savedStateHandle: SavedStateHandle
): ViewModel(){

    init {
        savedStateHandle.get<String>(Constants.DBN_ID)?.let { dbn ->
            getSchoolDetails(dbn)
        }

    }
    private val _state = mutableStateOf(SchoolDetailState())
    val state: State<SchoolDetailState> = _state

    private fun getSchoolDetails(dbn: String){
        getSchoolDetailUseCase(dbn).onEach { result ->
            when(result){
                is Resource.Success -> {
                    _state.value = SchoolDetailState(schoolDetail = result.data?.let { it.first() })
                }
                is Resource.Error -> {
                    _state.value = SchoolDetailState(error = result.message ?: "An Unexpected error occured")
                }
                else -> {}
            }
        }.launchIn(viewModelScope)
    }



}