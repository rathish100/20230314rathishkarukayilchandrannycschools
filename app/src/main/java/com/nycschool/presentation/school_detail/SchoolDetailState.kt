package com.nycschool.presentation.school_detail

import com.nycschool.domain.model.SchoolDetail

data class SchoolDetailState(
    val isLoading: Boolean = false,
    val schoolDetail: SchoolDetail? = null,
    val error: String = ""
)
