package com.nycschool.data.remote.dto

import com.google.gson.annotations.SerializedName
import com.nycschool.domain.model.School

data class SchoolDTO(
    @SerializedName("dbn")
    val dbn: String,
    @SerializedName("num_of_sat_test_takers")
    val numOfSatTestTakers: String,
    @SerializedName("sat_critical_reading_avg_score")
    val satCriticalReadingAvgScore: String,
    @SerializedName("sat_math_avg_score")
    val satMathAvgScore: String,
    @SerializedName("sat_writing_avg_score")
    val satWritingAvgScore: String,
    @SerializedName("school_name")
    val schoolName: String
)

fun SchoolDTO.toSchool(): School {
    return School(dbn = dbn,
    numOfSatTestTakers = numOfSatTestTakers,
    satCriticalReadingAvgScore = satCriticalReadingAvgScore,
    satMathAvgScore = satMathAvgScore,
    satWritingAvgScore = satWritingAvgScore,
    schoolName = schoolName)
}