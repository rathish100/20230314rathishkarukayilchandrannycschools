package com.nycschool.data.remote.dto


import com.google.gson.annotations.SerializedName
import com.nycschool.domain.model.SchoolDetail

data class SchoolDetailDTO(
    @SerializedName("academicopportunities1")
    val academicopportunities1: String,
    @SerializedName("academicopportunities2")
    val academicopportunities2: String,
    @SerializedName("academicopportunities3")
    val academicopportunities3: String,
    @SerializedName("academicopportunities4")
    val academicopportunities4: String,
    @SerializedName("academicopportunities5")
    val academicopportunities5: String,
    @SerializedName("addtl_info1")
    val addtlInfo1: String,
    @SerializedName("admissionspriority11")
    val admissionspriority11: String,
    @SerializedName("admissionspriority13")
    val admissionspriority13: String,
    @SerializedName("advancedplacement_courses")
    val advancedplacementCourses: String,
    @SerializedName("attendance_rate")
    val attendanceRate: String,
    @SerializedName("bbl")
    val bbl: String,
    @SerializedName("bin")
    val bin: String,
    @SerializedName("boro")
    val boro: String,
    @SerializedName("borough")
    val borough: String,
    @SerializedName("building_code")
    val buildingCode: String,
    @SerializedName("bus")
    val bus: String,
    @SerializedName("census_tract")
    val censusTract: String,
    @SerializedName("city")
    val city: String,
    @SerializedName("code1")
    val code1: String,
    @SerializedName("code2")
    val code2: String,
    @SerializedName("code3")
    val code3: String,
    @SerializedName("college_career_rate")
    val collegeCareerRate: String,
    @SerializedName("community_board")
    val communityBoard: String,
    @SerializedName("council_district")
    val councilDistrict: String,
    @SerializedName("dbn")
    val dbn: String,
    @SerializedName("eligibility2")
    val eligibility2: String,
    @SerializedName("ell_programs")
    val ellPrograms: String,
    @SerializedName("end_time")
    val endTime: String,
    @SerializedName("extracurricular_activities")
    val extracurricularActivities: String,
    @SerializedName("fax_number")
    val faxNumber: String,
    @SerializedName("finalgrades")
    val finalgrades: String,
    @SerializedName("grade9geapplicants1")
    val grade9geapplicants1: String,
    @SerializedName("grade9geapplicants2")
    val grade9geapplicants2: String,
    @SerializedName("grade9geapplicants3")
    val grade9geapplicants3: String,
    @SerializedName("grade9geapplicantsperseat1")
    val grade9geapplicantsperseat1: String,
    @SerializedName("grade9geapplicantsperseat2")
    val grade9geapplicantsperseat2: String,
    @SerializedName("grade9geapplicantsperseat3")
    val grade9geapplicantsperseat3: String,
    @SerializedName("grade9gefilledflag1")
    val grade9gefilledflag1: String,
    @SerializedName("grade9gefilledflag2")
    val grade9gefilledflag2: String,
    @SerializedName("grade9gefilledflag3")
    val grade9gefilledflag3: String,
    @SerializedName("grade9swdapplicants1")
    val grade9swdapplicants1: String,
    @SerializedName("grade9swdapplicants2")
    val grade9swdapplicants2: String,
    @SerializedName("grade9swdapplicants3")
    val grade9swdapplicants3: String,
    @SerializedName("grade9swdapplicantsperseat1")
    val grade9swdapplicantsperseat1: String,
    @SerializedName("grade9swdapplicantsperseat2")
    val grade9swdapplicantsperseat2: String,
    @SerializedName("grade9swdapplicantsperseat3")
    val grade9swdapplicantsperseat3: String,
    @SerializedName("grade9swdfilledflag1")
    val grade9swdfilledflag1: String,
    @SerializedName("grade9swdfilledflag2")
    val grade9swdfilledflag2: String,
    @SerializedName("grade9swdfilledflag3")
    val grade9swdfilledflag3: String,
    @SerializedName("grades2018")
    val grades2018: String,
    @SerializedName("graduation_rate")
    val graduationRate: String,
    @SerializedName("interest1")
    val interest1: String,
    @SerializedName("interest2")
    val interest2: String,
    @SerializedName("interest3")
    val interest3: String,
    @SerializedName("language_classes")
    val languageClasses: String,
    @SerializedName("latitude")
    val latitude: String,
    @SerializedName("location")
    val location: String,
    @SerializedName("longitude")
    val longitude: String,
    @SerializedName("method1")
    val method1: String,
    @SerializedName("method2")
    val method2: String,
    @SerializedName("method3")
    val method3: String,
    @SerializedName("neighborhood")
    val neighborhood: String,
    @SerializedName("nta")
    val nta: String,
    @SerializedName("overview_paragraph")
    val overviewParagraph: String,
    @SerializedName("pct_stu_enough_variety")
    val pctStuEnoughVariety: String,
    @SerializedName("pct_stu_safe")
    val pctStuSafe: String,
    @SerializedName("phone_number")
    val phoneNumber: String,
    @SerializedName("prgdesc1")
    val prgdesc1: String,
    @SerializedName("prgdesc2")
    val prgdesc2: String,
    @SerializedName("prgdesc3")
    val prgdesc3: String,
    @SerializedName("primary_address_line_1")
    val primaryAddressLine1: String,
    @SerializedName("program1")
    val program1: String,
    @SerializedName("program2")
    val program2: String,
    @SerializedName("program3")
    val program3: String,
    @SerializedName("psal_sports_boys")
    val psalSportsBoys: String,
    @SerializedName("psal_sports_girls")
    val psalSportsGirls: String,
    @SerializedName("requirement1_3")
    val requirement13: String,
    @SerializedName("requirement2_3")
    val requirement23: String,
    @SerializedName("requirement3_3")
    val requirement33: String,
    @SerializedName("requirement4_3")
    val requirement43: String,
    @SerializedName("requirement5_3")
    val requirement53: String,
    @SerializedName("requirement6_3")
    val requirement63: String,
    @SerializedName("school_10th_seats")
    val school10thSeats: String,
    @SerializedName("school_email")
    val schoolEmail: String,
    @SerializedName("school_name")
    val schoolName: String,
    @SerializedName("seats101")
    val seats101: String,
    @SerializedName("seats102")
    val seats102: String,
    @SerializedName("seats103")
    val seats103: String,
    @SerializedName("seats9ge1")
    val seats9ge1: String,
    @SerializedName("seats9ge2")
    val seats9ge2: String,
    @SerializedName("seats9ge3")
    val seats9ge3: String,
    @SerializedName("seats9swd1")
    val seats9swd1: String,
    @SerializedName("seats9swd2")
    val seats9swd2: String,
    @SerializedName("seats9swd3")
    val seats9swd3: String,
    @SerializedName("start_time")
    val startTime: String,
    @SerializedName("state_code")
    val stateCode: String,
    @SerializedName("subway")
    val subway: String,
    @SerializedName("total_students")
    val totalStudents: String,
    @SerializedName("website")
    val website: String,
    @SerializedName("zip")
    val zip: String
)

fun SchoolDetailDTO.toSchoolDetail(): SchoolDetail {
    return SchoolDetail(
        dbn = dbn,
        schoolName = schoolName,
        schoolEmail = schoolEmail,
        faxNumber = faxNumber,
        overviewParagraph = overviewParagraph,
        phoneNumber = phoneNumber,
        website = website)
}