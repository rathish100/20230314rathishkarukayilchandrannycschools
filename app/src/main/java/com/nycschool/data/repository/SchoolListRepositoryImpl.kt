package com.nycschool.data.repository

import com.nycschool.data.remote.NYCSchoolAPIService
import com.nycschool.data.remote.dto.SchoolDTO
import com.nycschool.data.remote.dto.SchoolDetailDTO
import com.nycschool.domain.repository.SchoolListRepository
import javax.inject.Inject

class SchoolListRepositoryImpl @Inject constructor(
    private val api: NYCSchoolAPIService
) : SchoolListRepository {

    override suspend fun getSchoolList(): List<SchoolDTO> {
        return api.getSchoolList()
    }

    override suspend fun getSchoolDetail(dbn: String): List<SchoolDetailDTO> {
        return api.getSchoolDetail(dbn)
    }

}