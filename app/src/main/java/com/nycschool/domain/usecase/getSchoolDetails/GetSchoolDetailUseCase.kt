package com.nycschool.domain.usecase.getSchoolDetails

import com.nycschool.data.remote.dto.toSchoolDetail
import com.nycschool.domain.model.SchoolDetail
import com.nycschool.domain.repository.SchoolListRepository
import com.nycschool.utils.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

//
class GetSchoolDetailUseCase @Inject constructor(
    private val repository: SchoolListRepository
) {
    operator fun invoke(dbn: String): Flow<Resource<List<SchoolDetail>>> = flow {
        try {
            emit(Resource.Loading())
            val schoolDetail = repository.getSchoolDetail(dbn).map {
                it.toSchoolDetail()
            }
            if (schoolDetail.isNotEmpty()) emit(Resource.Success(schoolDetail)) else {
                emit(Resource.Error("No Information available for given DBN $dbn"))
            }

        } catch (e: HttpException) {
            emit(Resource.Error(e.localizedMessage ?: "An unexpected error occured"))
        } catch (ex: IOException) {
            emit(Resource.Error("Couldn't reach server. Check your internet connection."))
        }
    }
}