package com.nycschool.utils

object Constants {
    const val BASE_URL = "https://data.cityofnewyork.us/resource/"
    const val DBN_ID = "dbn"
}