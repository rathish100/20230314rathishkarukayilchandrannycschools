package com.nycschool.di

import com.nycschool.data.repository.SchoolListRepositoryImpl
import com.nycschool.domain.repository.SchoolListRepository
import com.nycschool.utils.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NYCSchoolModule {
    @Provides
    @Singleton
    fun provideNYCSchoolApi(): com.nycschool.data.remote.NYCSchoolAPIService {
        return Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(com.nycschool.data.remote.NYCSchoolAPIService::class.java)
    }

    @Provides
    @Singleton
    fun provideSchoolListRepository(apiService: com.nycschool.data.remote.NYCSchoolAPIService): SchoolListRepository {
        return SchoolListRepositoryImpl(apiService)
    }

}