package com.nycschool.presentation.school_list

import okhttp3.HttpUrl
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before

 class SchoolListViewModelTest{

     private lateinit var schoolListViewModel: SchoolListViewModel
     private var server = MockWebServer()

     @Before
     fun setup(){
         server.start(port = 8080)
         val baseUrl: HttpUrl = server.url("/v1/")
         //schoolListViewModel = SchoolListViewModel()
     }

     @After
     fun tearDown() {
         server.shutdown()
     }

 }
